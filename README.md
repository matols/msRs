# msRs

R语言实现的股票数据分析Web框架。借助于shiny的Web框架，以及quantmod的数据模型进行R的数据可视化量化交易平台。

Rstudio主界面：
![输入图片说明](https://gitee.com/uploads/images/2018/0116/140859_72d8b2d9_125168.png "WX20180116-140732.png")


基于shiny框架的Web页面：
![输入图片说明](https://gitee.com/uploads/images/2018/0116/141041_660e4ad2_125168.png "WX20180116-140454.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0116/141102_674d0942_125168.png "WX20180116-140511.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0116/141119_f6ba86d3_125168.png "WX20180116-140700.png")

整体上基于R进行了简要的Web数据呈现，可在基础上进行二次开发。
